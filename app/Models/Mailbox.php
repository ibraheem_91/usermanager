<?php

namespace App\Model;

use Nitro\Failable;
use Nitro\Models\BaseMoloquent;

/**
 * This is the model class for collection 'primary_account'
 *
 * @property \MongoId|string $_id
 * @property mixed           $first_name
 * @property mixed           $last_name
 * @property mixed           $email
 * @property mixed           $contextio_id
 **/
class PrimaryAccount extends BaseMoloquent
{
    use Failable;

    /**
     * @var string
     */
    protected $collection = 'primary_account';

    /**
     * @var string
     */
    protected $connection = 'mongodb2';
}