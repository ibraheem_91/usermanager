<?php

namespace App\Model;

use Firebase\JWT\JWT as FireBaseJWT;

class JWT
{
    const JWT_SECRET_KEY = 'nekjlfwy84ocnlaknefwli32yo8hcilnle';

    public function getToken($primaryAccountId)
    {
        $key   = static::JWT_SECRET_KEY;
        $token = [
            "iss"                => env('APP_URL'),
            "aud"                => env('APP_URL'),
            "iat"                => time(),
            "nbf"                => time(),
            "exp"                => strtotime('now +60 mins'),
            "primary_account_id" => $primaryAccountId,
        ];
        $jwt   = FireBaseJWT::encode($token, $key, 'HS256');

        return $jwt;
    }

    /**
     * DESC
     *
     * @param string $jwtToken
     *
     * @return object
     *
     * @author Ibraheem Abu Kaff <ibra.abukaff@tajawal.com>
     *
     */
    public function validate($jwtToken = '')
    {
        FireBaseJWT::$leeway = 60;
        $key                 = static::JWT_SECRET_KEY;
        $decoded             = FireBaseJWT::decode($jwtToken, $key, ['HS256']);

        return $decoded;
    }

}