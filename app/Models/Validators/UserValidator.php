<?php

namespace App\Model\Validators;

use App\Core\Constants;

class UserValidator
{
    /**
     * DESC
     *
     * @param string $scenario
     *
     * @return array
     *
     * @author Ibraheem Abu Kaff <ibra.abukaff@tajawal.com>
     *
     */
    public static function getValidationRulesByScenario($scenario = '')
    {
        switch ($scenario) {
            case Constants::CREATE_NEW_USER:
                return [
                    Constants::FIRST_NAME => 'required|string|min:2|max:100',
                    Constants::LAST_NAME  => 'required|string|min:2|max:100',
                    Constants::EMAIL      => 'required|email|unique:attachment_api_users',
                    Constants::PASSWORD   => 'required|string|min:5|max:20',
                ];
                break;
            case Constants::LOGIN:
                return [
                    Constants::EMAIL    => 'required|email',
                    Constants::PASSWORD => 'required|string|min:5|max:20',
                ];
                break;
            default:
                return [];
        }
    }
}
