<?php

namespace App\Model;

use Nitro\Failable;
use Nitro\Models\BaseMoloquent;

/**
 * This is the model class for collection 'mailbox'.
 *
 * @property \MongoId|string $_id
 * @property mixed           $primary_account_id
 * @property mixed           $contextio_token
 * @property mixed           $mailbox_data
 **/
class Mailbox extends BaseMoloquent
{
    use Failable;

    /**
     * @var string
     */
    protected $collection = 'mailbox';

    /**
     * @var string
     */
    protected $connection = 'mongodb2';
}