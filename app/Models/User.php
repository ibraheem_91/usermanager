<?php

namespace App\Model;


use __;
use App\Core\Constants;
use App\Core\ExceptionMessage;
use App\Model\Validators\UserValidator;
use Nitro\Failable;
use Nitro\Helpers\Validator;
use Nitro\Models\BaseMoloquent;

/**
 * Class User
 *
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $password
 * @property array  $data
 * @package App\Model
 */
class User extends BaseMoloquent
{
    use Failable;

    /**
     * @var string
     */
    protected $collection = 'attachment_api_users';

    /**
     * @var string
     */
    protected $connection = 'mongodb';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password', 'data',
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    /**
     * DESC
     *
     * @param array $userData
     *
     * @return array
     *
     * @author Ibraheem Abu Kaff <ibra.abukaff@tajawal.com>
     *
     */
    public function createNewUser(array $userData = [])
    {
        //create user
        $this->first_name = __::get($userData, Constants::FIRST_NAME);
        $this->last_name  = __::get($userData, Constants::LAST_NAME);
        $this->password   = md5(sha1(__::get($userData, Constants::PASSWORD)));
        $this->email      = __::get($userData, Constants::EMAIL);
        $this->data       = __::get($userData, Constants::DATA, []);

        $this->save();
        $data = $this->getAttributes();

        return $data;
    }

    /**
     * DESC
     *
     * @return $this
     * @throws \Nitro\Exceptions\NotFoundException
     *
     * @author Ibraheem Abu Kaff <ibra.abukaff@tajawal.com>
     *
     */
    public function IsExistingByEmail()
    {
        $user = static::where('email', $this->email);
        if (!$user->count()) {
            $this->errorBadRequest(ExceptionMessage::USER_ACCOUNT_NOT_FOUND, ExceptionMessage::USER_ACCOUNT_ERROR);
        }

        $data = $user->first();

        return $data;
    }

    public function checkIfExistsByEmailAndPassword()
    {
        $this->IsExistingByEmail();
        $password = md5(sha1($this->password));
        $user     = static::where('email', $this->email)
                          ->where('password', $password);
        if (!$user->count()) {
            $this->errorBadRequest(ExceptionMessage::WRONG_CREDENTIALS, ExceptionMessage::USER_ACCOUNT_ERROR);
        }

        $data = $user->first();

        return $data;
    }

    /**
     * DESC
     *
     * @param array $newData
     *
     * @return array
     * @throws \Nitro\Exceptions\HttpException
     * @throws \Nitro\Exceptions\NotFoundException
     *
     * @author Ibraheem Abu Kaff <ibra.abukaff@tajawal.com>
     *
     */
    public function updateUser($newData = [])
    {
        $userId = __::get($newData, 'id', '');
        if (empty($userId)) {
            $this->errorBadRequest(ExceptionMessage::USER_ACCOUNT_NOT_FOUND, ExceptionMessage::USER_ACCOUNT_ERROR);
        }
        $user = static::where('_id', $userId);
        if (!$user->count()) {
            $this->errorBadRequest(ExceptionMessage::USER_ACCOUNT_NOT_FOUND, ExceptionMessage::USER_ACCOUNT_ERROR);
        }

        //security purpose, prevent any user from updating his data which must be set by us
        if (isset($newData['data'])) {
            unset($newData['data']);
        }

        if ($user->update($newData)) {
            $data = $user->first();

            return $data;
        }

        $this->errorBadRequest(ExceptionMessage::USER_UPDATE_ERROR, ExceptionMessage::USER_ACCOUNT_ERROR);
    }

    /**
     * DESC
     *
     * @param        $data
     * @param string $scenario
     *
     * @return array
     *
     * @author Ibraheem Abu Kaff <ibra.abukaff@tajawal.com>
     *
     */
    public function validateData($data, $scenario = '')
    {
        // Make a new validator object
        $validator = Validator::make($data, UserValidator::getValidationRulesByScenario($scenario));
        if ($validator->fails()) {
            $errors = json_encode($validator->errors()->getMessageBag()->getMessages());
            $this->errorInvalidParameters($errors, ExceptionMessage::USER_ACCOUNT_VALIDATION_ERROR);
        }

        return true;
    }
}