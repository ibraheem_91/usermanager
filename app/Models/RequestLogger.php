<?php

namespace App\Model;

use Illuminate\Http\Request;
use Nitro\Models\BaseMoloquent;

/**
 * Class RequestLogger
 *
 * @package App\Models
 */
class RequestLogger extends BaseMoloquent
{

    /**
     * @var string
     */
    protected $collection = 'all_request_log';

    /**
     * @var string
     */
    protected $connection = 'mongodb';

    /**
     * RequestLogger constructor.
     */
    public function __construct()
    {

    }

    /**
     * Logs the incoming request to database
     *
     * @param \Illuminate\Http\Request $request
     */
    public function logRequest(Request $request)
    {
        $toInsert = [
            'ip'            => $request->ip(),
            'url'           => $request->fullUrl(),
            'method'        => $request->getMethod(),
            'requestParams' => $request->all(),
            'headers'       => $request->header(),
        ];

        $this->insert($toInsert);
    }


}
