<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/


$app->get('/', function () use ($app) {
    return 'Hi';
});

$app->options('{all:.*}', ['middleware' => '\App\Http\Middleware\LumenCors', function () {
    return response('OK');
}]);

//@todo: https://github.com/palanik/lumen-cors
$app->group(
    ['prefix' => 'account', 'namespace' => 'App\Http\Controllers'],
    function () use ($app) {
        $app->post('/', ['middleware' => '\App\Http\Middleware\LumenCors', 'uses' => 'AccountController@createAccount', 'as' => 'account.createAccount']);

        $app->post('/login', ['middleware' => '\App\Http\Middleware\LumenCors', 'uses' => 'AccountController@loginToAccount', 'as' => 'account.loginToAccount']);

        $app->get('/connectTokenCallbackURL', ['middleware' => '\App\Http\Middleware\LumenCors', 'uses' => 'AccountController@connectTokenCallbackURL', 'as' => 'account.connectTokenCallbackURL']);

        $app->get('/active-mailboxes/{primaryAccountId}', ['middleware' => ['\App\Http\Middleware\LumenCors', '\App\Http\Middleware\JWTProtectionMiddleware'], 'uses' => 'AccountController@getAllMailboxes', 'as' => 'account.getAllMailboxes']);

        $app->post('/mailbox', ['middleware' => ['App\Http\Middleware\LumenCors', '\App\Http\Middleware\JWTProtectionMiddleware'], 'uses' => 'AccountController@createMailBox', 'as' => 'account.createMailBox']);

        $app->get('/sync-active-mailboxes/{primaryAccountId}', ['middleware' => ['\App\Http\Middleware\LumenCors', '\App\Http\Middleware\JWTProtectionMiddleware'], 'uses' => 'AccountController@SyncAllMailboxes', 'as' => 'account.SyncAllMailboxes']);

        $app->post('/attachments/{primaryAccountId}', ['middleware' => ['\App\Http\Middleware\LumenCors', '\App\Http\Middleware\JWTProtectionMiddleware'], 'uses' => 'AccountController@retrieveAttachments', 'as' => 'account.retrieveAttachments']);

        $app->get('/attachments/{primaryAccountId}/files/{file_id}', ['middleware' => ['\App\Http\Middleware\LumenCors', '\App\Http\Middleware\JWTProtectionMiddleware'], 'uses' => 'AccountController@downloadAttachment', 'as' => 'account.downloadAttachment']);


    });

$app->group(
    ['prefix' => 'utility', 'namespace' => 'App\Http\Controllers'],
    function () use ($app) {

        $app->get('/mimetypes', ['middleware' => ['\App\Http\Middleware\LumenCors', '\App\Http\Middleware\JWTProtectionMiddleware'], 'uses' => 'AccountController@getMimes', 'as' => 'account.utility.getMimes']);

    });