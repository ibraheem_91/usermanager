<?php

namespace App\Http\Middleware;

use __;
use App\Model\RequestLogger;
use Closure;
use FastRoute\Dispatcher;
use Illuminate\Http\Request;

/**
 * Class RequestLoggerMiddleware
 *
 * @package App\Http\Middleware
 */
class RequestLoggerMiddleware
{
    /**
     * @var \App\Model\RequestLogger
     */
    private $logger;

    public function __construct(RequestLogger $logger)
    {
        $this->logger = $logger;
    }

    /**
     * Logs the incoming requests
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (env('HTTP_REQUESTS_LOGGING', false) !== false) {
            $this->logger->logRequest($request);
        }

        return $next($request);
    }
}
