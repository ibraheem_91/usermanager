<?php

namespace App\Http\Middleware;

use __;
use App\Core\ExceptionMessage;
use App\Model\JWT;
use Closure;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\SignatureInvalidException;
use Illuminate\Http\Request;
use Nitro\Failable;

/**
 * Class JWTProtectionMiddleware
 *
 * @package App\Http\Middleware
 */
class JWTProtectionMiddleware
{
    use Failable;

    /**
     * DESC
     *
     * @todo   : what's the action for expired token
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     *
     * @author Ibraheem Abu Kaff <ibra.abukaff@tajawal.com>
     *
     */
    public function handle(Request $request, Closure $next)
    {

        $input = $request->all();

        $token = __::get($input, 'token', '');
        if (empty($token)) {
            $authorization    = $request->header('Authorization', '');
            $authorizationArr = explode(' ', $authorization);
            $isBearer         = __::get($authorizationArr, 0, '');
            if ($isBearer == 'Bearer') {
                $token = __::get($authorizationArr, 1, '');
            } else {
                $token = '';
            }

        }
        if (empty($token)) {
            $this->errorUnauthorized(ExceptionMessage::UNAUTHORIZED, ExceptionMessage::UNAUTHORIZED);
        }
        try {
            $jwt     = new JWT();
            $payload = $jwt->validate($token);
        } catch (ExpiredException $expiredException) {
            $this->errorUnauthorized($expiredException->getMessage(), ExceptionMessage::UNAUTHORIZED);
        } catch (SignatureInvalidException $signatureException) {
            $this->errorUnauthorized($signatureException->getMessage(), ExceptionMessage::UNAUTHORIZED);
        } catch (\Exception $e) {
            $this->errorUnauthorized($e->getMessage(), ExceptionMessage::UNAUTHORIZED);
        }

        return $next($request);
    }
}