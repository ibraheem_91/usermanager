<?php

namespace App\Http\Controllers;

/**
 * Class Controller
 *
 * @package App\Http\Controllers
 */
class Controller extends \Nitro\Controllers\BaseController
{

    /**
     * Controller constructor.
     */
    public function __construct()
    {
    }
}
