<?php

namespace App\Http\Controllers;

use __;
use App\Core\Constants;
use App\Core\ExceptionMessage;
use App\Core\Helpers;
use App\Model\JWT;
use App\Model\User;
use Illuminate\Http\Request;

class AccountController extends Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * DESC
     *
     * @todo   : write CRON job to delete users created 2 days back and the don't have data.primary_account_id,think about reflecting these deletion on attachment api
     * @todo   : what if the user leaves the steps , when he comes later he can't register with his account
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Laravel\Lumen\Http\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     *
     * @author Ibraheem Abu Kaff <ibra.abukaff@tajawal.com>
     *
     */
    public function createAccount(Request $request)
    {
        //get request params
        //validate using create new user scenario
        //send request to attachment api to create primary account
        //return response from attachment api as it is

        $params  = $request->all();
        $user    = new User();
        $isValid = $user->validateData($params, Constants::CREATE_NEW_USER);
        if ($isValid) {
            $createdUser       = $user->createNewUser($params);
            $params            = [
                'first_name' => __::get($createdUser, Constants::FIRST_NAME),
                'last_name'  => __::get($createdUser, Constants::LAST_NAME),
                'email'      => __::get($createdUser, Constants::EMAIL),
            ];
            $attachmentManager = Helpers::getAttachmentManagerHandler();
            $response          = $attachmentManager->createAccount($params);

            return $this->respond($response);

        }
    }

    /**
     * DESC
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Laravel\Lumen\Http\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Nitro\Exceptions\HttpException
     *
     * @author Ibraheem Abu Kaff <ibra.abukaff@tajawal.com>
     *
     */
    public function loginToAccount(Request $request)
    {
        $params  = $request->all();
        $user    = new User();
        $isValid = $user->validateData($params, Constants::LOGIN);
        if ($isValid) {
            $user->email      = __::get($params, Constants::EMAIL, '');
            $user->password   = __::get($params, Constants::PASSWORD, '');
            $userData         = $user->checkIfExistsByEmailAndPassword();
            $primaryAccountId = __::get($userData, 'data.primary_account_id', '');
            //@todo : handle the error , what's the case we don't have primary_account_id
            if (empty($primaryAccountId)) {
                $this->errorBadRequest(ExceptionMessage::UNABLE_TO_LOGIN, ExceptionMessage::USER_ACCOUNT_ERROR);
            }
            //generate JWT Token
            $jwtModel = new JWT();
            $jwt      = $jwtModel->getToken($primaryAccountId);
            $data     = ['token' => $jwt];

            return ($this->respond($data));
        }
    }

    /**
     * DESC
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Laravel\Lumen\Http\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     *
     * @author Ibraheem Abu Kaff <ibra.abukaff@tajawal.com>
     *
     */
    public function connectTokenCallbackURL(Request $request)
    {
        $params            = $request->all();
        $connectToken      = __::get($params, 'contextio_token', '');
        $attachmentManager = Helpers::getAttachmentManagerHandler();
        $response          = $attachmentManager->connectTokenCallbackUrl($connectToken);
        $primaryAccountId  = __::get($response, 'primary_account_id');

        $alreadyStored = User::where('data.primary_account_id', $primaryAccountId)->count();
        if (!$alreadyStored) {
            $dataToAdd       = [
                'primary_account_id' => $primaryAccountId,
            ];
            $RegisteredEmail = __::get($response, 'mailbox_data.email', '');
            //check if the user is there
            $user        = new User();
            $user->email = $RegisteredEmail;
            $userByEmail = $user->IsExistingByEmail();
            $userByEmail->update(['data' => $dataToAdd]);
        }
        //generate JWT Token
        $jwtModel = new JWT();
        $jwt      = $jwtModel->getToken($primaryAccountId);
        $data     = ['token' => $jwt];

        return ($this->respond($data));

    }

    /**
     * DESC
     *
     * @param $primaryAccountId
     *
     * @return \Laravel\Lumen\Http\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     *
     * @author Ibraheem Abu Kaff <ibra.abukaff@tajawal.com>
     *
     */
    public function getAllMailboxes($primaryAccountId)
    {
        $attachmentManager = Helpers::getAttachmentManagerHandler();
        $activeMailboxes   = $attachmentManager->getActiveMailboxes($primaryAccountId);

        return $this->respond($activeMailboxes);

    }

    /**
     * DESC
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Laravel\Lumen\Http\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     *
     * @author Ibraheem Abu Kaff <ibra.abukaff@tajawal.com>
     *
     */
    public function createMailBox(Request $request)
    {
        $params            = $request->all();
        $attachmentManager = Helpers::getAttachmentManagerHandler();
        $response          = $attachmentManager->createMailbox($params);

        return $this->respond($response);
    }

    /**
     * DESC
     *
     * @param $primaryAccountId
     *
     * @return \Laravel\Lumen\Http\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     *
     * @author Ibraheem Abu Kaff <ibra.abukaff@tajawal.com>
     *
     */
    public function SyncAllMailboxes($primaryAccountId)
    {
        $attachmentManager = Helpers::getAttachmentManagerHandler();
        $response          = $attachmentManager->syncActiveMailboxes($primaryAccountId);

        return $this->respond($response);

    }

    /**
     * DESC
     *
     * @param                          $primaryAccountId
     * @param \Illuminate\Http\Request $request
     *
     * @return \Laravel\Lumen\Http\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     *
     * @author Ibraheem Abu Kaff <ibra.abukaff@tajawal.com>
     *
     */
    public function retrieveAttachments($primaryAccountId, Request $request)
    {
        $params            = $request->all();
        $attachmentManager = Helpers::getAttachmentManagerHandler();
        $response          = $attachmentManager->retrieveAttachments($params, $primaryAccountId);

        return $this->respond($response);
    }

    /**
     * DESC
     *
     * @param $primaryAccountId
     * @param $fileId
     *
     * @return \Laravel\Lumen\Http\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     *
     * @author Ibraheem Abu Kaff <ibra.abukaff@tajawal.com>
     *
     */
    public function downloadAttachment($primaryAccountId, $fileId)
    {
        $attachmentManager = Helpers::getAttachmentManagerHandler();
        $response          = $attachmentManager->downloadAttachment($primaryAccountId, $fileId);

        return $this->respond($response);
    }

    /**
     * DESC
     *
     * @return \Laravel\Lumen\Http\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     *
     * @author Ibraheem Abu Kaff <ibra.abukaff@tajawal.com>
     *
     */
    public function getMimes()
    {
        $attachmentManager = Helpers::getAttachmentManagerHandler();
        $response          = $attachmentManager->retrieveMimeTypes();

        return $this->respond($response);
    }

}
