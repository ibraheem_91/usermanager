<?php

namespace App\Console\Commands;

use App\Model\Mailbox;
use App\Model\PrimaryAccount;
use App\Model\User;
use Carbon\Carbon;
use Illuminate\Console\Command;

class UserCleaner extends Command
{
    const LIMIT = 100;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user-cleaner:clean';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clean In-active users';

    /**
     * SystemCache constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     * Delete the users which are in active for more than 1 day
     *
     * @todo: delete later any further user based data, this script was written when we have three collections(primary_account,mailbox, and attachment_api_users) that store email data info
     * @todo: when the use can't continue his registration let's display him a message that he should try after 10 minutes
     *
     * @return mixed
     */
    public function handle()
    {
        $count             = PrimaryAccount::all()->count();
        $limit             = static::LIMIT;
        $skip              = 0;
        $numberOfIteration = intval(ceil($count / $limit));
        for ($i = 0; $i < $numberOfIteration; $i++) {
            $primaryAccounts = PrimaryAccount::skip($skip)->take($limit)->get(['email', 'contextio_id', 'created_at']);
            foreach ($primaryAccounts as $primaryAccount) {
                $primaryAccountEmail = empty($primaryAccount->email) ? '' : $primaryAccount->email;
                $contextIoId         = empty($primaryAccount->contextio_id) ? '' : $primaryAccount->contextio_id;
                $createdAt           = empty($primaryAccount->created_at) ? '' : $primaryAccount->created_at;
                if (empty($primaryAccountEmail) || empty($contextIoId) || empty($createdAt)) {
                    continue;
                }

                $createdAtInSeconds = strtotime((string)($createdAt));
                $now                = strtotime('now');
                $diff               = Carbon::createFromTimestamp($createdAtInSeconds)->diffInMinutes(Carbon::createFromTimestamp($now));
                //when the difference between the time of account creation is within 10 mins, then leav this account
                // as it could be continuing the registration, otherwise delete the account
                if ($diff <= 10) {
                    continue;
                }
                $emptyObject = new \stdClass();
                //take their mailboxes and delete them
                Mailbox::where('mailbox_data', $emptyObject)
                       ->where('primary_account_id', $contextIoId)->delete();
                //delete the primary account
                PrimaryAccount::where('contextio_id', $contextIoId)
                              ->where('email', $primaryAccountEmail)->delete();
                //delete the user account
                User::where('email', $primaryAccountEmail)
                    ->where('data', $emptyObject)->delete();

            }
            $skip += $limit;
        }
    }
}
