<?php

namespace App\Core;

use ibraheem_91\AttachmentManager\Clients\AttachmentManager;
use ibraheem_91\AttachmentManager\GuzzleHandler;
use Illuminate\Http\Request;

class Helpers
{
    /**
     * DESC
     *
     * @return \ibraheem_91\AttachmentManager\Clients\AttachmentManager
     *
     * @author Ibraheem Abu Kaff <ibra.abukaff@tajawal.com>
     *
     */
    public static function getAttachmentManagerHandler()
    {
        $baseUrl           = 'http://attachmanager.ibra-node.com';
        $token             = 'BLKu3ZWRWw5UqtBxnuUOMPgarU4pwzIS';
        $handler           = new GuzzleHandler($baseUrl, [
            'headers' => [
                Constants::X_CLIENT_HEADERS => json_encode(Request::capture()->header()),
                Constants::X_AUTH_TOKEN     => $token,
            ],
        ]);
        $attachmentManager = new AttachmentManager($handler);

        return $attachmentManager;
    }
}