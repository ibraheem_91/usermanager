<?php

namespace App\Core;

class Constants
{
    const FIRST_NAME = 'first_name';
    const LAST_NAME  = 'last_name';
    const EMAIL      = 'email';
    const PASSWORD   = 'password';
    const DATA       = 'data';

    const CREATE_NEW_USER = 'create_new_user';
    const LOGIN           = 'login';

    const X_AUTH_TOKEN     = 'x-auth-token';
    const X_CLIENT_HEADERS = 'x-client-headers';
}