<?php

namespace App\Core;

class ExceptionMessage
{
    //Main Messages in Exceptions(Exception's title)
    const USER_ACCOUNT_ERROR            = 'USER_ACCOUNT_ERROR';
    const USER_ACCOUNT_VALIDATION_ERROR = 'USER_ACCOUNT_VALIDATION_ERROR';

    //Exception details
    //BadRequest
    const USER_ACCOUNT_NOT_FOUND = 'USER_ACCOUNT_NOT_FOUND';
    const UNABLE_TO_LOGIN        = 'UNABLE_TO_LOGIN';
    const USER_UPDATE_ERROR      = 'USER_UPDATE_ERROR';
    const EMPTY_TOKEN            = 'EMPTY_TOKEN';
    const UNAUTHORIZED           = 'UNAUTHORIZED';
    const WRONG_CREDENTIALS      = 'WRONG_CREDENTIALS';


}